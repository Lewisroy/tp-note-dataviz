﻿<!DOCTYPE html>
<html>
	<head>
		<title>Data Vizualisation - TP1</title>
		<!-- Inclusion CSS (librairie + perso) -->
		<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css">
		<link rel="stylesheet" type="text/css" href="css/dataviz.css">
		
		<!-- Inclusion JS (librairie + scripts de création de graph) -->
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.jqplot.min.js"></script>

		<script type="text/javascript" src="js/renderer/jqplot.pieRenderer.js"></script>
		<script type="text/javascript" src="js/renderer/jqplot.barRenderer.js"></script>
		<script type="text/javascript" src="js/renderer/jqplot.categoryAxisRenderer.js"></script>
		<script type="text/javascript" src="js/renderer/jqplot.dateAxisRenderer.js"></script>
		<script type="text/javascript" src="js/renderer/jqplot.barRenderer.js"></script>
		<script type="text/javascript" src="js/renderer/jqplot.categoryAxisRenderer.js"></script>
		<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
		<script src="https://www.amcharts.com/lib/3/serial.js"></script>
		<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
		<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type=text/css" media="all" />
		<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
		
		<script type="text/javascript" src="js/dataviz.js"></script>
		<style type="text/css">
			#chartdiv {
				width		: 100%;
				height		: 500px;
				font-size	: 11px;
			}			
		</style>
	</head>
	<body>
		<?php include ('structure/header.php'); ?>
		<div id="content">
		<h1>Question 1 </h1>
		<div class="plot" id="q1"></div>
		<h1>Question 2 </h1>
		<div class="plot" id="q2"></div>
		<h1>Question 6 </h1>
		<div id="chartdiv"></div>	

		srgdery
			
		</div>
		<?php include ('structure/footer.php'); ?>
	</body>
</html>