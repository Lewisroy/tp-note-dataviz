$(document).ready(function(){
	// Pas de cache sur les requête IMPORTANT !
	$.ajaxSetup({ cache: false });
	
	/*** 
		On définit ici les fonctions de base qui vont nous servir à la récupération des données
		Je ne définis que le GET ici, mais il est possible d'utiliser POST pour récupérer ses données (on le verra dans un prochain TP)
	****/
	
	
	/***************************************
		QUESTION 6 : PIE CHART : Notes selon sexe
	****************************************/
	submitForm();
});


var getFormName = function(){
	if($('#user').length){
		return $('#user').val();
	}
};

var getFormSexe = function(){
	if($('.radio-btn').length){
		return $('.radio-btn:checked').val();
	}
}


var getRequest = function(url, callback) {
	$.get(url, function(data) {
		data = $.parseJSON(data);
		callback(data);
	});
};

var submitForm = function(){
	if($('#submit').length){
		$('#submit').on('click', function(){
			var sexe = getFormSexe();
			var user = getFormName();
			var dataForGraph = [
				 ['1', 0],
				 ['2', 0],
				 ['3', 0],
				 ['4', 0],
				 ['5', 0]
			];
			getRequest("webservices/notations_user_sexe.php?user="+user+"&sexe="+sexe, function(data) {
				for(var i = 0; i<data.length; i++){
					dataForGraph[data[i][0]-1][1]++;

				}
			});
			generatePie(dataForGraph);
		});
	}
}

var generatePie = function(data){
	console.log(data);
	var plot4 = $.jqplot('chart1', [[["1",data[0][1]],["2",data[1][1]], ["3", data[2][1]], ["4", data[3][1]], ["5", data[4][1]]]], {
        seriesDefaults:{
            renderer:$.jqplot.PieRenderer, 
            rendererOptions:{ 
            	sliceMargin: 0,
            	showDataLabels: true
            }
        },
        legend:{ show: true }      
    });
};
   
  