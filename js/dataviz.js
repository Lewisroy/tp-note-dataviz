﻿$(document).ready(function(){
	// Pas de cache sur les requête IMPORTANT !
	$.ajaxSetup({ cache: false });
	
	/*** 
		On définit ici les fonctions de base qui vont nous servir à la récupération des données
		Je ne définis que le GET ici, mais il est possible d'utiliser POST pour récupérer ses données (on le verra dans un prochain TP)
	****/
	function getRequest(url, callback) {
		$.get(url, function(data) {
			data = $.parseJSON(data);
			callback(data);
		});
	}

	function generateDateAxis(idDiv, data) {
		//var line1=[['2008-06-30 8:00AM',4], ['2008-7-14 8:00AM',6.5], ['2008-7-28 8:00AM',5.7], ['2008-8-11 8:00AM',9], ['2008-8-25 8:00AM',8.2]];
		var plot2 = $.jqplot(idDiv, [data], {
			title:'', 
			axes:{
        xaxis:{
            renderer:$.jqplot.DateAxisRenderer
        }
    },
			series:[{lineWidth:4, markerOptions:{style:'square'}}]
		}).replot();
	}


	function generateGroupBarChart(idDiv, data) {
		plotgroupBar = $.jqplot(idDiv, [data], {
            seriesDefaults: {
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
                shadowAngle: 135,
                rendererOptions: {
                    barDirection: 'horizontal'
                }
            },
            axes: {
                yaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer
                }
            },
			legend: {
                show: true,
                location: 'ne',
                placement: 'inside',
				labels: data[0]
            },
        });
	}

	/**************************************
	*********************************************************************/
	getRequest("webservices/friends_bydaymonth.php?user=1", function(data) {
        generateDateAxis("q1", data);
        console.log(data);
        console.log("coucou");
    });

    getRequest("webservices/notations_bydaypermonth.php?user=1", function(data) {
        generateDateAxis("q2", data);
        console.log(data);
        console.log("coucou");
    });

    getRequest("webservices/groupBar.php?user=2", function(data) {
		 console.log(data);
		 var chart = AmCharts.makeChart("chartdiv", {
	"type": "serial",
     "theme": "light",
	"categoryField": "year",
	"rotate": true,
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start",
		"position": "left"
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonText": "Income:[[value]]",
			"fillAlphas": 0.8,
			"id": "AmGraph-1",
			"lineAlpha": 0.2,
			"title": "Income",
			"type": "column",
			"valueField": "income"
		},
		{
			"balloonText": "Expenses:[[value]]",
			"fillAlphas": 0.8,
			"id": "AmGraph-2",
			"lineAlpha": 0.2,
			"title": "Expenses",
			"type": "column",
			"valueField": "expenses"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"position": "top",
			"axisAlpha": 0
		}
	],
	"allLabels": [],
	"balloon": {},
	"titles": [],
	"dataProvider": [
		{
			"year": "18-21",
			"income": data[0][0],
			"expenses": data[0][1]
		},
		{
			"year": "22-25",
			"income": data[1][0],
			"expenses": data[1][1]
		},
		{
			"year": "26-29",
			"income": data[2][0],
			"expenses": data[2][1]
		}
	],
    "export": {
    	"enabled": true
     }

});
	});







});