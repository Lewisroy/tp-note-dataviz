<?php
	// Le tableau de résultat
	$result_request = array();
	
	/*
		On teste si le paramètre GET existe
		0 -> tous les utilisateurs

		SELECT `notations.note` FROM `notations` LEFT JOIN `utilisateurs` WHERE `notations.photo`=35 AND `utilisateur.sexe`=1

		id_unique -> un seul utilisateur
		plusieurs id séparés par des virgules -> plusieurs utilisateurs

		SELECT n.note FROM notations n Join utilisateurs u on n.noteur = u.id WHERE n.photo=35 AND u.sexe=1
	*/
	if(isset($_GET['user'])) {
		// Connexion à la BDD
		include("../bdd/connexion_bdd.php");
		
		$user = $_GET['user'];
		$sexe = $_GET['sexe'];
	
		$query = "SELECT n.note FROM notations n JOIN utilisateurs u on n.noteur = u.id";
		if($user != 0) {
			$query = $query." WHERE n.photo=". $user ." AND u.sexe=". $sexe;
		}
		$query = $query." ORDER BY n.note ASC";
		
		$result = mysqli_query($conn, $query);
	
		while ($row = mysqli_fetch_array($result)) {
			$result_request[] = array(intval($row[0]));
		}

		mysqli_free_result($result);
	
		// Déconnexion de la BDD
		include("../bdd/deconnexion_bdd.php");
	}
	
	// Renvoyer le résultat au javascript
	echo json_encode($result_request);

?>