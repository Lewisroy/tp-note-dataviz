<?php




	$result_request = array();
	
	/*
		On teste si le paramètre GET existe
		0 -> tous les utilisateurs
		id_unique -> un seul utilisateur
		plusieurs id séparés par des virgules -> plusieurs utilisateurs
	*/
	if(isset($_GET['user'])) {
		// Connexion à la BDD
		include("../bdd/connexion_bdd.php");
		
		$user = $_GET['user'];
	
		$query = "SELECT noteur, photo, note, date
				FROM notations";
		if($user != 0) {
			$query = $query." WHERE photo IN (".$user.")";
		}
		
		$result = mysqli_query($conn, $query);
	
		while ($row = mysqli_fetch_array($result)) {
			$result_request[] = array(intval($row[0]), $row[1], $row[2], $row[3]);
		}

		mysqli_free_result($result);
	
		// Déconnexion de la BDD
		include("../bdd/deconnexion_bdd.php");
	}
	




	$returNotePerDate = array();
	$nbNoteThisDay = 0;
	$date = new DateTime('first day of this month');
	$date = $date->modify(" -1 year");

	for ($i = 0; $i < 30; $i ++ ) {
		$data = array();
		//On définit le jour
		array_push($data, $date->format("Y-m-d"));
		$date->modify("+1 day");
		// Get les datas du jour
		
		$nb = 0;
		$note = 0;
		foreach ($result_request as $key => $case) {
			if($case[3] == $date->format("Y-m-d"))
			{
				$nb++;
				$note = $note + $case[2];
			}
		}
		if($nb !=0)
			$nbNoteThisDay += ceil($note/$nb);

		//On ajoute la valeur au tableau
		array_push($data, $nbNoteThisDay);
		array_push($returNotePerDate, $data);
	}

	echo json_encode($returNotePerDate);


?>