<?php
	// Le tableau de résultat
	$result_request = array();
	
	/*
		On teste si le paramètre GET existe
		0 -> tous les utilisateurs
		id_unique -> un seul utilisateur
		plusieurs id séparés par des virgules -> plusieurs utilisateurs
	*/
	if(isset($_GET['user'])) {
		// Connexion à la BDD
		include("../bdd/connexion_bdd.php");
		
		$user = $_GET['user'];
	
		$query = "SELECT user1, user2, date
				FROM relations";
		if($user != 0) {
			$query = $query." WHERE user1 IN (".$user.")";
		}
		
		$result = mysqli_query($conn, $query);
	
		while ($row = mysqli_fetch_array($result)) {
			$result_request[] = array(intval($row[0]), intval($row[1]), $row[2]);
		}

		mysqli_free_result($result);
	
		// Déconnexion de la BDD

	}
	

	$classify = array();
	$data = array();
	$t1_f = 0;
	$t1_h = 0;
	$t2_f = 0;
	$t2_h = 0;
	$t3_f = 0;
	$t3_h = 0;

	foreach ($result_request as $key => $val) {
		// pour chaque utilisateur on get les data
		$user = $val[1];
		$query = "SELECT id, age, sexe
				FROM utilisateurs WHERE id IN (".$user.")";
		
		
		$result = mysqli_query($conn, $query);
	
		
		

		while ($row = mysqli_fetch_array($result)) {
			if( intval($row[1]) >= 18 &&  intval($row[1])<=21 ){
				if( intval($row[2] == 0))
					$t1_f += $t1_f;
				if( intval($row[2] == 1))
					$t1_h = $t1_h + 1;
			}

			if( intval($row[1]) >= 22 &&  intval($row[1])<=25 ){
				if( intval($row[2] == 0))
					$t2_f = $t2_f + 1;
				if( intval($row[2] == 1))
					$t2_h = $t2_h + 1;
			}
			if( intval($row[1]) >= 26 &&  intval($row[1])<=29 ){
				if( intval($row[2] == 0))
					$t3_f = $t3_f + 1;

				if( intval($row[2] == 1))
					$t3_h = $t3_h + 1;
			}
			
		}

		mysqli_free_result($result);

	}

	array_push($classify, array($t1_f,$t1_h));
	array_push($classify, array($t2_f,$t2_h));
	array_push($classify, array($t3_f,$t3_h));
	// Renvoyer le résultat au javascript
	echo json_encode($classify);

?>