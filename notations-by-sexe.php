<!DOCTYPE html>
<html>
	<head>
		<title>Data Vizualisation - TP1</title>
		<!-- Inclusion CSS (librairie + perso) -->
		<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css">
		<link rel="stylesheet" type="text/css" href="css/dataviz.css">
		
		<!-- Inclusion JS (librairie + scripts de création de graph) -->
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.jqplot.min.js"></script>
		<script type="text/javascript" src="js/renderer/jqplot.pieRenderer.js"></script>
		<script type="text/javascript" src="js/renderer/jqplot.donutRenderer.js"></script>
		
		
		<script type="text/javascript" src="js/notations_user_sexe.js"></script>
	</head>
	<body>
		<?php include ('structure/header.php'); ?>
		<div id="content">
			<h1>Popularité de votre profil pour un sexe donné par tranche de notation (pour chaque note possible, de 1 à 5)</h1>
			<div class="form-container">
				<input type="number" name="id" id="user"/>	
			</div>
			<div class="form-container">
				<input class="radio-btn" type="radio" name="gender" value="0" checked> Homme<br>
				<input class="radio-btn" type="radio" name="gender" value="1"> Femme<br>
			</div>
			<input id="submit" type="submit" value="Générer le graph" />
		</div>

		<div id="chart1"></div>
		<?php include ('structure/footer.php'); ?>
	</body>
</html>